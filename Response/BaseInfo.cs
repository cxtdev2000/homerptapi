﻿using System;
namespace IdentityServer.Response
{
    public class BaseInfo
    {
        public int Code { get; set; } = -1;
        /// <summary>
        /// Mã trả về
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// Kết quả trả về
        /// </summary>
        public string JsonData { get; set; }
        /// <summary>
        /// 
        /// </summary>
    }
}

