﻿using System;
using IdentityServer.ObjectShare;
using IdentityServer.Models;
using System.Collections.Generic;

namespace IdentityServer.Response
{
    public class AdministrativeRegionResponse : SearchInquiryResponse
    {
        public List<AdministrativeRegion> Datas { get; set; } = new List<AdministrativeRegion>();
    }
}

