﻿using System;
using IdentityServer.ObjectShare;
using IdentityServer.Models;
using System.Collections.Generic;

namespace IdentityServer.Response
{
    public class AspNetRoleResponse : SearchInquiryResponse
    {
        public List<AspNetRole> Datas { get; set; } = new List<AspNetRole>();
    }
}

