﻿using System;
using IdentityServer.ObjectShare;
using IdentityServer.Models;
using System.Collections.Generic;

namespace IdentityServer.Response
{
    public class DistrictResponse : SearchInquiryResponse
    {
        public List<District> Datas { get; set; } = new List<District>();
    }
}

