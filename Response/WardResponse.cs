﻿using System;
using IdentityServer.ObjectShare;
using IdentityServer.Models;
using System.Collections.Generic;

namespace IdentityServer.Response
{
    public class WardResponse : SearchInquiryResponse
    {
        public List<Ward> Datas { get; set; } = new List<Ward>();
    }
}

