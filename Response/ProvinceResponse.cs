﻿using System;
using IdentityServer.ObjectShare;
using IdentityServer.Models;
using System.Collections.Generic;

namespace IdentityServer.Response
{
    public class ProvinceResponse : SearchInquiryResponse
    {
        public List<Province> Datas { get; set; } = new List<Province>();
    }
}

