﻿using System;
using IdentityServer.ObjectShare;
using IdentityServer.Models;
using System.Collections.Generic;

namespace IdentityServer.Response
{
    public class UserInquiryResponse : SearchInquiryResponse
    {
        public List<AspNetUser> Datas { get; set; } = new List<AspNetUser>();
    }
}

