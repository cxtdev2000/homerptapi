﻿using System;
using IdentityServer.ObjectShare;
using IdentityServer.Models;
using System.Collections.Generic;

namespace IdentityServer.Response
{
    public class GroupResponse : SearchInquiryResponse
    {
        public List<Group> Datas { get; set; } = new List<Group>();
    }
}

