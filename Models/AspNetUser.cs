﻿using System;
using System.Collections.Generic;

namespace IdentityServer.Models
{
    public partial class AspNetUser
    {
        public AspNetUser()
        {
            AspNetUserClaims = new HashSet<AspNetUserClaim>();
            AspNetUserLogins = new HashSet<AspNetUserLogin>();
            AspNetUserTokens = new HashSet<AspNetUserToken>();
            UserGroups = new HashSet<UserGroup>();
            Roles = new HashSet<AspNetRole>();
        }

        public string Id { get; set; }
        public int AccessFailedCount { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public bool LockoutEnabled { get; set; }
        public DateTimeOffset? LockoutEnd { get; set; }
        public string NormalizedEmail { get; set; }
        public string NormalizedUserName { get; set; }
        public string PasswordHash { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public string SecurityStamp { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public int? Age { get; set; }
        public int? IdWard { get; set; }
        public int? IdProvince { get; set; }
        public string Address { get; set; }
        public int? IdDistrict { get; set; }
        public DateTime? Birthdate { get; set; }
        public bool? Active { get; set; }
        public bool Deleted { get; set; }
        public string Code { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public string Role { get; set; }
        public string RoleName { get; set; }

        public virtual ICollection<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual ICollection<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual ICollection<AspNetUserToken> AspNetUserTokens { get; set; }
        public virtual ICollection<UserGroup> UserGroups { get; set; }

        public virtual ICollection<AspNetRole> Roles { get; set; }
    }
}
