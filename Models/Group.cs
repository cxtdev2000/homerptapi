﻿using System;
using System.Collections.Generic;

namespace IdentityServer.Models
{
    public partial class Group
    {
        public Group()
        {
            UserGroups = new HashSet<UserGroup>();
        }

        /// <summary>
        /// mã
        /// 
        /// </summary>
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }

        public virtual ICollection<UserGroup> UserGroups { get; set; }
    }
}
