﻿using System;
using System.Collections.Generic;

namespace IdentityServer.Models
{
    public partial class UserGroup
    {
        public string IdGroup { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public string LastUpdateBy { get; set; }
        public string IdUser { get; set; }

        public virtual Group IdGroupNavigation { get; set; }
        public virtual AspNetUser IdUserNavigation { get; set; }
    }
}
