﻿using System;
using System.Collections.Generic;

namespace IdentityServer.Models
{
    public partial class Search
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Tltxcd { get; set; }
        public string Title { get; set; }
        public string Titleen { get; set; }
        public string Keyword { get; set; }
        public string Storename { get; set; }
        public string Exporttype { get; set; }
        public string Description { get; set; }
        public bool? Deleted { get; set; }
        public string Createby { get; set; }
        public DateTime? Createdate { get; set; }
        public string Lastchangeby { get; set; }
        public DateTime? Lastchangedate { get; set; }
    }
}
