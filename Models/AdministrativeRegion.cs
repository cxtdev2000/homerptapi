﻿using System;
using System.Collections.Generic;

namespace IdentityServer.Models
{
    public partial class AdministrativeRegion
    {
        public AdministrativeRegion()
        {
            Provinces = new HashSet<Province>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string NameEn { get; set; }
        public string CodeName { get; set; }
        public string CodeNameEn { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }

        public virtual ICollection<Province> Provinces { get; set; }
    }
}
