﻿using System;
using System.Collections.Generic;

namespace IdentityServer.Models
{
    public partial class Searchfld
    {
        public int Id { get; set; }
        public string Searchcode { get; set; }
        public string Fldcode { get; set; }
        public string Fldname { get; set; }
        public string Fldnameen { get; set; }
        public int? Position { get; set; }
        public string Operator { get; set; }
        public string Display { get; set; }
        public string Isrecordkey { get; set; }
        public int? Width { get; set; }
        public int? Expwidth { get; set; }
        public string Allowsort { get; set; }
        public string Align { get; set; }
        public string Fixed { get; set; }
        public string Datatype { get; set; }
        public string Srch { get; set; }
        public string Source { get; set; }
        public string Sqlcmd { get; set; }
        public string Defvalue { get; set; }
        public string Control { get; set; }
        public string Createby { get; set; }
        public DateTime? Createdate { get; set; }
        public string Lastchangeby { get; set; }
        public DateTime? Lastchangedate { get; set; }
    }
}
