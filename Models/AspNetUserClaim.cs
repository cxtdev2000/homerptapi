﻿using System;
using System.Collections.Generic;

namespace IdentityServer.Models
{
    public partial class AspNetUserClaim
    {
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
        public string UserId { get; set; }
        public int Id { get; set; }

        public virtual AspNetUser User { get; set; }
    }
}
