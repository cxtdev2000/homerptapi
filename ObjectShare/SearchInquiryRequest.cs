﻿using System;
using System.Collections.Generic;

namespace IdentityServer.ObjectShare
{
    public class SearchInquiryRequest
    {
        public int IsExport { get; set; } = 0;
        public List<Conditions> conds { get; set; }
        public int pageNum { get; set; } = 3;
        public int pageSize { get; set; } = 20;
        //sortConds: [],
    }
    public class Conditions
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public string Operator { get; set; }
    }
}

