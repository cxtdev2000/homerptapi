﻿//using System;
//using IdentityServer.Models;
//using IdentityServer4;
//using IdentityServer.Data;

//namespace IdentityServer.ObjectShare
//{
//    // I'm specifying the TKey generic param here since we use int's for our DB keys
//    // you may need to customize this for your environment
//    public class MyUserStore : IUserLockoutStore<ApplicationUser, int>
//    {
//        // IUserStore implementation here

//        public Task<DateTimeOffset> GetLockoutEndDateAsync(MyUser user)
//        {
//            //..
//        }

//        public Task SetLockoutEndDateAsync(MyUser user, DateTimeOffset lockoutEnd)
//        {
//            //..
//        }

//        public Task<int> IncrementAccessFailedCountAsync(MyUser user)
//        {
//            //..
//        }

//        public Task ResetAccessFailedCountAsync(MyUser user)
//        {
//            //..
//        }

//        public Task<int> GetAccessFailedCountAsync(MyUser user)
//        {
//            //..
//        }

//        public Task<bool> GetLockoutEnabledAsync(MyUser user)
//        {
//            //..
//        }

//        public Task SetLockoutEnabledAsync(MyUser user, bool enabled)
//        {
//            //..
//        }
//    }
//}

