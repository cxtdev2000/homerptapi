﻿using System;
namespace IdentityServer.ObjectShare
{
    public class SearchInquiryResponse
    {
        public SearchInquiryResponse()
        {
        }
        public int RecordCount { get; set; } = 0;
        public int PageCount { get; set; } = 0;
        public int PageSize { get; set; } = 20;
        public int PageNum { get; set; } = 1;
    }
}

