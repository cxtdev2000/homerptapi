using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IdentityServer.Data;
using IdentityServer.Models;
using IdentityServer.Response;
using IdentityServer.Request;

using IdentityModel;
using IdentityServer.Data;
using IdentityServer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System.Security.Claims;
using Serilog;
using Microsoft.Extensions.Configuration;
using IdentityServer.ObjectShare;

namespace IdentityServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GroupController : ControllerBase
    {
        private readonly MainContext _context;

        public GroupController(MainContext context)
        {
            _context = context;
        }

        // GET: api/Group
        [HttpGet]
        public async Task<ActionResult<List<Group>>> Get()
        {
            return await _context.Groups.ToListAsync();
        }
        [HttpPost]
        public async Task<ActionResult<GroupResponse>> GetGroups(SearchInquiryRequest req)
        {
            var datas = _context.Groups
                .AsEnumerable()
                .Where(e =>
                (!req.conds.Any(x => x.Key == "name") || e.Name.Contains(req.conds.Where(x => x.Key == "name").Select(e => e.Value).FirstOrDefault()))
                )
                .ToList();

            //foreach(var cond in req.conds)
            //{
            //    if(cond.Key == "code") datas = datas.Where(e => e.Code == )
            //}

            var count = datas.Count();


            datas = datas.OrderByDescending(e => e.CreateDate).Skip((req.pageNum - 1) * req.pageSize).Take(req.pageSize)
                .ToList();

            GroupResponse rs = new GroupResponse()
            {
                RecordCount = count,
                PageCount = count / req.pageSize == 0 ? 1 : datas.Count() / 20,
                PageSize = req.pageSize,
                PageNum = req.pageNum,
                Datas = datas
            };

            return rs;
        }

        // GET: api/Group/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Group>> GetGroup(string id)
        {
            var Group = await _context.Groups.FindAsync(id);

            if (Group == null)
            {
                return NotFound();
            }

            return Group;
        }

        // PUT: api/Group/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("updateone")]
        public async Task<IActionResult> PutGroup(Group Group)
        {

            BaseInfo rs = new BaseInfo();

            _context.Entry(Group).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                rs.Code = 1;
            }
            catch (DbUpdateConcurrencyException e)
            {
                rs.Message = e.Message;
                return Ok(rs);
            }
            return Ok(rs);
        }

        // POST: api/Group
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("addone")]
        public async Task<IActionResult> PostGroup(Group Group)
        {
            BaseInfo rs = new BaseInfo();
            Group.CreateDate = DateTime.Now;
            try
            {
                _context.Groups.Add(Group);
                _context.SaveChanges();
                rs.Code = 1;
            }
            catch (DbUpdateException)
            {
                if (GroupExists(Group.Id))
                {
                    rs.Code = -1;
                    rs.Message = "Đã tồn tại dữ liệu trùng";
                    return Ok(rs);
                }
                else
                {
                    throw;
                }
            }
            return Ok(rs);
        }

        // DELETE: api/Group/5
        [HttpPost("deleteone")]
        public async Task<IActionResult> DeleteGroup(Group req)
        {
            BaseInfo rs = new BaseInfo();
            var Group = await _context.Groups.FindAsync(req.Id);
            if (Group == null)
            {
                rs.Code = -1;
            }

            _context.Groups.Remove(Group);
            await _context.SaveChangesAsync();

            rs.Code = 1;
            return Ok(rs);
        }

        private bool GroupExists(string id)
        {
            return _context.Groups.Any(e => e.Id == id);
        }
    }
}
