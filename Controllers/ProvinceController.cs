using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IdentityServer.Data;
using IdentityServer.Models;
using IdentityServer.Response;
using IdentityServer.Request;

using IdentityModel;
using IdentityServer.Data;
using IdentityServer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System.Security.Claims;
using Serilog;
using Microsoft.Extensions.Configuration;
using IdentityServer.ObjectShare;

namespace IdentityServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProvinceController : ControllerBase
    {
        private readonly VnRegionContext _context;

        public ProvinceController(VnRegionContext context)
        {
            _context = context;
        }

        // GET: api/Province
        [HttpGet]
        public async Task<ActionResult<List<Province>>> Get()
        {
            return await _context.Provinces.ToListAsync();
        }

        [HttpPost]
        public async Task<ActionResult<ProvinceResponse>> GetProvinces(SearchInquiryRequest req)
        {
            var datas = _context.Provinces
                .AsEnumerable()
                .Where(e =>
                (!req.conds.Any(x => x.Key == "code") || e.Code.Contains(req.conds.Where(x => x.Key == "code").Select(e => e.Value).FirstOrDefault())) &&
                (!req.conds.Any(x => x.Key == "fullName") || e.FullName.Contains(req.conds.Where(x => x.Key == "fullName").Select(e => e.Value).FirstOrDefault())) &&
                (!req.conds.Any(x => x.Key == "zipCode") || e.ZipCode.Contains(req.conds.Where(x => x.Key == "zipCode").Select(e => e.Value).FirstOrDefault()))
                )
                .ToList();

            //foreach(var cond in req.conds)
            //{
            //    if(cond.Key == "code") datas = datas.Where(e => e.Code == )
            //}

            var count = datas.Count();


            datas = datas.OrderByDescending(e => e.CreateDate).Skip((req.pageNum - 1) * req.pageSize).Take(req.pageSize)
                .Join(_context.AdministrativeRegions, a => a.AdministrativeRegionId, b => b.Id, (a, b) =>
                {
                    a.AdministrativeRegion = b;
                    return a;
                })
                .ToList();

            ProvinceResponse rs = new ProvinceResponse()
            {
                RecordCount = count,
                PageCount = count / req.pageSize == 0 ? 1 : datas.Count() / 20,
                PageSize = req.pageSize,
                PageNum = req.pageNum,
                Datas = datas
            };

            return rs;
        }

        // GET: api/Province/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Province>> GetProvince(string id)
        {
            var province = await _context.Provinces.FindAsync(id);

            if (province == null)
            {
                return NotFound();
            }

            return province;
        }

        // PUT: api/Province/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("updateone")]
        public async Task<IActionResult> PutProvince(Province province)
        {

            BaseInfo rs = new BaseInfo();

            _context.Entry(province).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                rs.Code = 1;
            }
            catch (DbUpdateConcurrencyException e)
            {
                rs.Message = e.Message;
                return Ok(rs);
            }
            return Ok(rs);
        }

        // POST: api/Province
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("addone")]
        public async Task<IActionResult> PostProvince(Province province)
        {
            BaseInfo rs = new BaseInfo();
            province.CreateDate = DateTime.Now;
            province.Name = province.FullName;
            try
            {
                _context.Provinces.Add(province);
                _context.SaveChanges();
                rs.Code = 1;
            }
            catch (DbUpdateException)
            {
                if (ProvinceExists(province.Code))
                {
                    rs.Code = -1;
                    rs.Message = "Đã tồn tại dữ liệu trùng";
                    return Ok(rs);
                }
                else
                {
                    throw;
                }
            }
            return Ok(rs);
        }

        // DELETE: api/Province/5
        [HttpPost("deleteone")]
        public async Task<IActionResult> DeleteProvince(Province req)
        {
            BaseInfo rs = new BaseInfo();
            var province = await _context.Provinces.FindAsync(req.Code);
            if (province == null)
            {
                rs.Code = -1;
            }

            _context.Provinces.Remove(province);
            await _context.SaveChangesAsync();

            rs.Code = 1;
            return Ok(rs);
        }

        private bool ProvinceExists(string id)
        {
            return _context.Provinces.Any(e => e.Code == id);
        }
    }
}
