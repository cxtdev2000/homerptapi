using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IdentityServer.Data;
using IdentityServer.Models;
using IdentityServer.Response;
using IdentityServer.Request;

using IdentityModel;
using IdentityServer.Data;
using IdentityServer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System.Security.Claims;
using Serilog;
using Microsoft.Extensions.Configuration;
using IdentityServer.ObjectShare;

namespace IdentityServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DistrictController : ControllerBase
    {
        private readonly VnRegionContext _context;

        public DistrictController(VnRegionContext context)
        {
            _context = context;
        }

        // GET: api/District
        [HttpGet]
        public async Task<ActionResult<List<District>>> Get()
        {
            return await _context.Districts.ToListAsync();
        }

        [HttpPost]
        public async Task<ActionResult<DistrictResponse>> GetDistricts(SearchInquiryRequest req)
        {
            var datas = _context.Districts
                .AsEnumerable()
                .Where(e =>
                (!req.conds.Any(x => x.Key == "code") || e.Code.Contains(req.conds.Where(x => x.Key == "code").Select(e => e.Value).FirstOrDefault())) &&
                (!req.conds.Any(x => x.Key == "fullName") || e.FullName.Contains(req.conds.Where(x => x.Key == "fullName").Select(e => e.Value).FirstOrDefault()))
                )
                .ToList();

            //foreach(var cond in req.conds)
            //{
            //    if(cond.Key == "code") datas = datas.Where(e => e.Code == )
            //}

            var count = datas.Count();


            datas = datas.OrderByDescending(e => e.CreateDate).Skip((req.pageNum - 1) * req.pageSize).Take(req.pageSize)
                .Join(_context.Provinces, a => a.ProvinceCode, b => b.Code, (a, b) =>
                {
                    a.ProvinceCodeNavigation = b;
                    return a;
                })
                .ToList();

            DistrictResponse rs = new DistrictResponse()
            {
                RecordCount = count,
                PageCount = count / req.pageSize == 0 ? 1 : datas.Count() / 20,
                PageSize = req.pageSize,
                PageNum = req.pageNum,
                Datas = datas
            };

            return rs;
        }

        // GET: api/District/5
        [HttpGet("{id}")]
        public async Task<ActionResult<District>> GetDistrict(string id)
        {
            var district = await _context.Districts.FindAsync(id);

            if (district == null)
            {
                return NotFound();
            }

            return district;
        }

        // PUT: api/District/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("updateone")]
        public async Task<IActionResult> PutDistrict(District district)
        {

            BaseInfo rs = new BaseInfo();

            _context.Entry(district).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                rs.Code = 1;
            }
            catch (DbUpdateConcurrencyException e)
            {
                rs.Message = e.Message;
                return Ok(rs);
            }
            return Ok(rs);
        }

        // POST: api/District
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("addone")]
        public async Task<IActionResult> PostDistrict(District district)
        {
            BaseInfo rs = new BaseInfo();
            district.CreateDate = DateTime.Now;
            district.Name = district.FullName;
            try
            {
                _context.Districts.Add(district);
                _context.SaveChanges();
                rs.Code = 1;
            }
            catch (DbUpdateException)
            {
                if (DistrictExists(district.Code))
                {
                    rs.Code = -1;
                    rs.Message = "Đã tồn tại dữ liệu trùng";
                    return Ok(rs);
                }
                else
                {
                    throw;
                }
            }
            return Ok(rs);
        }

        // DELETE: api/District/5
        [HttpPost("deleteone")]
        public async Task<IActionResult> DeleteDistrict(District req)
        {
            BaseInfo rs = new BaseInfo();
            var district = await _context.Districts.FindAsync(req.Code);
            if (district == null)
            {
                rs.Code = -1;
            }

            _context.Districts.Remove(district);
            await _context.SaveChangesAsync();

            rs.Code = 1;
            return Ok(rs);
        }

        private bool DistrictExists(string id)
        {
            return _context.Districts.Any(e => e.Code == id);
        }
    }
}
