using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IdentityServer.Data;
using IdentityServer.Models;
using IdentityServer.Response;
using IdentityServer.Request;

using IdentityModel;
using IdentityServer.Data;
using IdentityServer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System.Security.Claims;
using Serilog;
using Microsoft.Extensions.Configuration;
using IdentityServer.ObjectShare;
using IdentityServer.Extensions;

namespace IdentityServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly MainContext _context;
        public IConfiguration Configuration { get; }
        private UserManager<ApplicationUser> _userManager;

        public UserController(MainContext context, IConfiguration configuration, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
            Configuration = configuration;
        }


        [HttpPost]
        public async Task<ActionResult<UserInquiryResponse>> GetAspNetUsers(SearchInquiryRequest req)
        {
            var datas = _context.AspNetUsers
                .AsEnumerable()
                .Where(e => !e.Deleted &&
                (!req.conds.Any(x => x.Key == "code") || e.Code.Contains(req.conds.Where(x => x.Key == "code").Select(e => e.Value).FirstOrDefault())) &&
                (!req.conds.Any(x => x.Key == "fullName") || e.FullName.Contains(req.conds.Where(x => x.Key == "fullName").Select(e => e.Value).FirstOrDefault()))
                )
                .ToList();


            var count = datas.Count();


            datas = datas.OrderByDescending(e => e.CreateDate).Skip((req.pageNum - 1) * req.pageSize).Take(req.pageSize)
                .Join(_context.AspNetRoles, a => a.Role, b => b.NormalizedName, (a, b) =>
                {
                    a.RoleName = b.Name;
                    return a;
                })
                .ToList();

            UserInquiryResponse rs = new UserInquiryResponse()
            {
                RecordCount = count,
                PageCount = count / req.pageSize == 0 ? 1 : datas.Count() / 20,
                PageSize = req.pageSize,
                PageNum = req.pageNum,
                Datas = datas
            };

            return rs;
        }

        // POST: api/User/active
        [HttpPost("activeone")]
        public async Task<ActionResult<BaseInfo>> ChangeActiveAspNetUsers(UserRequest rq)
        {
            BaseInfo rs = new BaseInfo();

            try
            {
                var dts = _context.AspNetUsers.Where(e => e.Code == rq.Code).ToList();
                foreach (var dt in dts)
                {
                    dt.Active = rq.Active;
                }
                _context.SaveChanges();
                rs.Code = 1;
            }
            catch (Exception e)
            {
            }
            return rs;
        }

        // GET: api/User/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AspNetUser>> GetAspNetUser(string id)
        {
            var aspNetUser = await _context.AspNetUsers.FindAsync(id);

            if (aspNetUser == null)
            {
                return NotFound();
            }

            return aspNetUser;
        }


        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("updateone")]
        public async Task<ActionResult<BaseInfo>> EditAspNetUser(UserRequest aspNetUser)
        {
            BaseInfo rs = new BaseInfo();

            var identityUser = await _userManager.FindByIdAsync(aspNetUser.Id.ToString());



            //if (!result.Succeeded)
            //{
            //    throw new Exception(result.Errors.First().Description);
            //}

            //_ = await _userManager.UpdateAsync(identityUser);
            //// get the Password hashed
            //var newPassHash = _userManager.PasswordHasher.HashPassword(identityUser, aspNetUser.PasswordHash);

            //// Change the Username field
            //var usernameResult = await _userManager.SetUserNameAsync(identityUser, aspNetUser.UserName);

            //// Change the email field
            //_ = await _userManager.SetEmailAsync(identityUser, aspNetUser.Email);

            //// Remore old role
            //_ = await _userManager.RemoveFromRoleAsync(identityUser, await _context.AspNetUsers.Where(e => e.Id == aspNetUser.Id).Select(e => e.Role).FirstOrDefaultAsync());
            // Change the role
            //_ = await _userManager.AddToRoleAsync(identityUser, aspNetUser.Role);

            //_ = await _userManager.AddToRolesAsync(identityUser, new string[] { aspNetUser.Role });
            //_ = await _userManager.UpdateAsync(identityUser);

            // Remore old role then add new
            //_ = _userManager.RemoveClaimsAsync(identityUser, new Claim[] { new Claim(JwtClaimTypes.Role, await _context.AspNetUsers.Where(e => e.Id == aspNetUser.Id).Select(e => e.Role).FirstOrDefaultAsync()), }).Result;
            //_ = _userMan
            //_ = await _userManager.AddToRolesAsync(identityUser, new string[] { aspNetUser.Role });ager.AddClaimsAsync(identityUser, new Claim[]{
            //                new Claim(JwtClaimTypes.Role, aspNetUser.Role),
            //            }).Result;

            // Remore old role
            _ = await _userManager.RemoveFromRoleAsync(identityUser, await _context.AspNetUsers.Where(e => e.Id == aspNetUser.Id).Select(e => e.Role).FirstOrDefaultAsync());
            //  Change the role
            _ = await _userManager.AddToRoleAsync(identityUser, aspNetUser.Role);
            aspNetUser.RoleName = "";

            // Get the Password hashed
            aspNetUser.PasswordHash = _userManager.PasswordHasher.HashPassword(identityUser, aspNetUser.PasswordHash);
            _context.Entry(aspNetUser).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                rs.Code = 1;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AspNetUserExists(aspNetUser.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return rs;
        }

        // POST: api/User
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("addone")]
        public async Task<IActionResult> PostAspNetUser(UserRequest aspNetUser)
        {
            BaseInfo rs = new BaseInfo();

            try
            {
                rs = await CreateAppUser(aspNetUser);
            }
            catch (DbUpdateException)
            {
                if (AspNetUserExists(aspNetUser.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            //return CreatedAtAction("GetAspNetUser", new { id = aspNetUser.Id }, aspNetUser);
            return Ok(rs);
        }

        // DELETE: api/User/5
        [HttpPost("deleteone")]
        public async Task<IActionResult> DeleteAspNetUser(AspNetUser aspNetUser)
        {
            BaseInfo rs = new BaseInfo();

            var user = await _context.AspNetUsers.Where(e => e.Id == aspNetUser.Id).FirstOrDefaultAsync();
            if (aspNetUser == null) //return lỗi ko tìm thấy người dùng cần xoá
            {
                rs.Code = -1;
                rs.Message = "Không tìm thấy người dùng";
                return Ok(rs);
            }

            var identityUser = await _userManager.FindByIdAsync(aspNetUser.Id.ToString());

            //_ = await _userManager.SetLockoutEnabledAsync(identityUser, true);
            //_ = await _userManager.SetLockoutEndDateAsync(identityUser, DateTimeOffset.MaxValue);
            _ = await _userManager.DeleteAsync(identityUser);

            //_context.AspNetUsers.Remove(aspNetUser);
            //user.Deleted = true;

            //await _context.SaveChangesAsync();

            rs.Code = 1;
            return Ok(rs);
        }

        private bool AspNetUserExists(string id)
        {
            return _context.AspNetUsers.Any(e => e.Id == id);
        }

        private async Task<BaseInfo> CreateAppUser(AspNetUser aspNetUser)
        {
            BaseInfo rs = new BaseInfo();

            var services = new ServiceCollection();
            services.AddLogging();
            services.AddDbContext<ApplicationDbContext>(options =>
               options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            var serviceProvider = services.BuildServiceProvider();
            var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
            var userMgr = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();

            var newUser = new ApplicationUser
            {
                UserName = aspNetUser.UserName,
                Email = aspNetUser.Email,
                EmailConfirmed = true,
            };
            var result = userMgr.CreateAsync(newUser, aspNetUser.PasswordHash).Result;

            // Change the role
            _ = await userMgr.AddToRoleAsync(newUser, aspNetUser.Role);
            //_ = await _userManager.AddToRolesAsync(newUser, new string[] { aspNetUser.Role });

            if (!result.Succeeded)
            {
                return new BaseInfo()
                {
                    Code = -1,
                    Message = result.Errors.First().Description
                };
            }

            //result = userMgr.AddClaimsAsync(newUser, new Claim[]{
            //                new Claim(JwtClaimTypes.Name, aspNetUser.FullName),
            //                new Claim(JwtClaimTypes.Name, aspNetUser.FullName),
            //                new Claim(JwtClaimTypes.GivenName, aspNetUser.FullName),
            //                new Claim(JwtClaimTypes.Email, aspNetUser.Email),
            //                new Claim(JwtClaimTypes.PhoneNumber, aspNetUser.PhoneNumber),
            //                new Claim(JwtClaimTypes.PreferredUserName, aspNetUser.UserName),
            //                new Claim(JwtClaimTypes.Role, aspNetUser.Role),
            //            }).Result;

            if (!result.Succeeded)
            {
                throw new Exception(result.Errors.First().Description);
            }

            var userCreated = _context.AspNetUsers.Where(e => e.Id == newUser.Id).FirstOrDefault();
            var userLogon = await GetCurrentUserAsync();

            userCreated.Role = aspNetUser.Role;
            userCreated.RoleName = "";
            userCreated.FullName = aspNetUser.FullName;
            userCreated.PhoneNumber = aspNetUser.PhoneNumber;
            userCreated.Code = aspNetUser.Code;
            userCreated.Birthdate = aspNetUser.Birthdate;
            userCreated.Active = aspNetUser.Active;
            userCreated.CreateDate = DateTime.Now;
            userCreated.CreateBy = userLogon?.Id;

            _context.SaveChanges();
            //UserCreated.CreateBy = ;

            Log.Debug("user created");

            rs.Code = 1;
            return rs;
        }
        private Task<ApplicationUser> GetCurrentUserAsync() => _userManager.GetUserAsync(HttpContext.User);
    }
}
