using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IdentityServer.Data;
using IdentityServer.Models;
using IdentityServer.Response;
using IdentityServer.Request;

using IdentityModel;
using IdentityServer.Data;
using IdentityServer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System.Security.Claims;
using Serilog;
using Microsoft.Extensions.Configuration;
using IdentityServer.ObjectShare;

namespace IdentityServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdministrativeRegionController : ControllerBase
    {
        private readonly VnRegionContext _context;

        public AdministrativeRegionController(VnRegionContext context)
        {
            _context = context;
        }

        // GET: api/AdministrativeRegion
        [HttpGet]
        public async Task<ActionResult<List<AdministrativeRegion>>> Get()
        {
            return await _context.AdministrativeRegions.ToListAsync();
        }
        [HttpPost]
        public async Task<ActionResult<AdministrativeRegionResponse>> GetAdministrativeRegions(SearchInquiryRequest req)
        {
            var datas = _context.AdministrativeRegions
                .AsEnumerable()
                .Where(e =>
                (!req.conds.Any(x => x.Key == "name") || e.Name.Contains(req.conds.Where(x => x.Key == "name").Select(e => e.Value).FirstOrDefault()))
                )
                .ToList();

            //foreach(var cond in req.conds)
            //{
            //    if(cond.Key == "code") datas = datas.Where(e => e.Code == )
            //}

            var count = datas.Count();

            datas = datas.OrderByDescending(e => e.CreateDate).Skip((req.pageNum - 1) * req.pageSize).Take(req.pageSize).ToList();

            AdministrativeRegionResponse rs = new AdministrativeRegionResponse()
            {
                RecordCount = count,
                PageCount = count / req.pageSize == 0 ? 1 : datas.Count() / 20,
                PageSize = req.pageSize,
                PageNum = req.pageNum,
                Datas = datas
            };

            return rs;
        }

        // GET: api/AdministrativeRegion/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AdministrativeRegion>> GetAdministrativeRegion(string id)
        {
            var administrativeRegion = await _context.AdministrativeRegions.FindAsync(id);

            if (administrativeRegion == null)
            {
                return NotFound();
            }

            return administrativeRegion;
        }

        // PUT: api/AdministrativeRegion/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("updateone")]
        public async Task<IActionResult> PutAdministrativeRegion(AdministrativeRegion administrativeRegion)
        {

            BaseInfo rs = new BaseInfo();

            _context.Entry(administrativeRegion).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                rs.Code = 1;
            }
            catch (DbUpdateConcurrencyException e)
            {
                rs.Message = e.Message;
                return Ok(rs);
            }
            return Ok(rs);
        }

        // POST: api/AdministrativeRegion
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("addone")]
        public async Task<IActionResult> PostAdministrativeRegion(AdministrativeRegion administrativeRegion)
        {
            BaseInfo rs = new BaseInfo();
            administrativeRegion.CreateDate = DateTime.Now;
            try
            {
                _context.AdministrativeRegions.Add(administrativeRegion);
                _context.SaveChanges();
                rs.Code = 1;
            }
            catch (DbUpdateException)
            {
                if (AdministrativeRegionExists(administrativeRegion.Id))
                {
                    rs.Code = -1;
                    rs.Message = "Đã tồn tại dữ liệu trùng";
                    return Ok(rs);
                }
                else
                {
                    throw;
                }
            }
            return Ok(rs);
        }

        // DELETE: api/AdministrativeRegion/5
        [HttpPost("deleteone")]
        public async Task<IActionResult> DeleteAdministrativeRegion(AdministrativeRegion req)
        {
            BaseInfo rs = new BaseInfo();
            var administrativeRegion = await _context.AdministrativeRegions.FindAsync(req.Id);
            if (administrativeRegion == null)
            {
                rs.Code = -1;
            }

            _context.AdministrativeRegions.Remove(administrativeRegion);
            await _context.SaveChangesAsync();

            rs.Code = 1;
            return Ok(rs);
        }

        private bool AdministrativeRegionExists(int id)
        {
            return _context.AdministrativeRegions.Any(e => e.Id == id);
        }
    }
}
