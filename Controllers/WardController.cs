using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IdentityServer.Data;
using IdentityServer.Models;
using IdentityServer.Response;
using IdentityServer.Request;

using IdentityModel;
using IdentityServer.Data;
using IdentityServer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System.Security.Claims;
using Serilog;
using Microsoft.Extensions.Configuration;
using IdentityServer.ObjectShare;

namespace IdentityServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WardController : ControllerBase
    {
        private readonly VnRegionContext _context;

        public WardController(VnRegionContext context)
        {
            _context = context;
        }

        // GET: api/Ward
        [HttpGet]
        public async Task<ActionResult<List<Ward>>> Get()
        {
            return await _context.Wards.ToListAsync();
        }
        [HttpPost]
        public async Task<ActionResult<WardResponse>> GetWards(SearchInquiryRequest req)
        {
            var datas = _context.Wards
                .AsEnumerable()
                .Where(e =>
                (!req.conds.Any(x => x.Key == "code") || e.Code.Contains(req.conds.Where(x => x.Key == "code").Select(e => e.Value).FirstOrDefault())) &&
                (!req.conds.Any(x => x.Key == "fullName") || e.FullName.Contains(req.conds.Where(x => x.Key == "fullName").Select(e => e.Value).FirstOrDefault()))
                )
                .ToList();

            //foreach(var cond in req.conds)
            //{
            //    if(cond.Key == "code") datas = datas.Where(e => e.Code == )
            //}

            var count = datas.Count();


            datas = datas.OrderByDescending(e => e.CreateDate).Skip((req.pageNum - 1) * req.pageSize).Take(req.pageSize)
                .Join(_context.Districts, a => a.DistrictCode, b => b.Code, (a, b) =>
                {
                    a.DistrictCodeNavigation = b;
                    return a;
                })
                .Join(_context.Provinces, a => a.DistrictCodeNavigation.ProvinceCode, b => b.Code, (a, b) =>
                {
                    a.DistrictCodeNavigation.ProvinceCodeNavigation = b;
                    return a;
                })
                .ToList();

            WardResponse rs = new WardResponse()
            {
                RecordCount = count,
                PageCount = count / req.pageSize == 0 ? 1 : datas.Count() / 20,
                PageSize = req.pageSize,
                PageNum = req.pageNum,
                Datas = datas
            };

            return rs;
        }

        // GET: api/Ward/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Ward>> GetWard(string id)
        {
            var ward = await _context.Wards.FindAsync(id);

            if (ward == null)
            {
                return NotFound();
            }

            return ward;
        }

        // PUT: api/Ward/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("updateone")]
        public async Task<IActionResult> PutWard(Ward ward)
        {

            BaseInfo rs = new BaseInfo();

            _context.Entry(ward).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                rs.Code = 1;
            }
            catch (DbUpdateConcurrencyException e)
            {
                rs.Message = e.Message;
                return Ok(rs);
            }
            return Ok(rs);
        }

        // POST: api/Ward
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("addone")]
        public async Task<IActionResult> PostWard(Ward ward)
        {
            BaseInfo rs = new BaseInfo();
            ward.CreateDate = DateTime.Now;
            ward.Name = ward.FullName;
            try
            {
                _context.Wards.Add(ward);
                _context.SaveChanges();
                rs.Code = 1;
            }
            catch (DbUpdateException)
            {
                if (WardExists(ward.Code))
                {
                    rs.Code = -1;
                    rs.Message = "Đã tồn tại dữ liệu trùng";
                    return Ok(rs);
                }
                else
                {
                    throw;
                }
            }
            return Ok(rs);
        }

        // DELETE: api/Ward/5
        [HttpPost("deleteone")]
        public async Task<IActionResult> DeleteWard(Ward req)
        {
            BaseInfo rs = new BaseInfo();
            var ward = await _context.Wards.FindAsync(req.Code);
            if (ward == null)
            {
                rs.Code = -1;
            }

            _context.Wards.Remove(ward);
            await _context.SaveChangesAsync();

            rs.Code = 1;
            return Ok(rs);
        }

        private bool WardExists(string id)
        {
            return _context.Wards.Any(e => e.Code == id);
        }
    }
}
