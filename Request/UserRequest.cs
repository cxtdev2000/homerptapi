﻿using System;
using System.Collections.Generic;
using IdentityServer.Models;

namespace IdentityServer.Request
{
    public class UserRequest : AspNetUser
    {
        public List<string> Id_groups { get; set; }
    }
}

