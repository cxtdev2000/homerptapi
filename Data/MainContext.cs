﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using IdentityServer.Models;

namespace IdentityServer.Data
{
    public partial class MainContext : DbContext
    {
        public MainContext()
        {
        }

        public MainContext(DbContextOptions<MainContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetRoleClaim> AspNetRoleClaims { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUserToken> AspNetUserTokens { get; set; }
        public virtual DbSet<Group> Groups { get; set; }
        public virtual DbSet<Search> Searches { get; set; }
        public virtual DbSet<Searchfld> Searchflds { get; set; }
        public virtual DbSet<UserGroup> UserGroups { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Data Source=localhost;Initial Catalog=main;User Id=sa;Password=k5U2&1EYoCQ&;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AspNetRole>(entity =>
            {
                entity.HasIndex(e => e.NormalizedName, "RoleNameIndex")
                    .IsUnique();

                entity.Property(e => e.Name).HasMaxLength(256);

                entity.Property(e => e.NormalizedName).HasMaxLength(256);
            });

            modelBuilder.Entity<AspNetRoleClaim>(entity =>
            {
                entity.HasIndex(e => e.RoleId, "IX_AspNetRoleClaims_RoleId");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.RoleId).IsRequired();

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetRoleClaims)
                    .HasForeignKey(d => d.RoleId);
            });

            modelBuilder.Entity<AspNetUser>(entity =>
            {
                entity.HasIndex(e => e.NormalizedEmail, "EmailIndex");

                entity.HasIndex(e => e.NormalizedUserName, "UserNameIndex")
                    .IsUnique();

                entity.HasIndex(e => new { e.Code, e.Deleted }, "code_unique")
                    .IsUnique();

                entity.HasIndex(e => new { e.UserName, e.Deleted }, "username_unique")
                    .IsUnique();

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.Birthdate).HasColumnType("datetime");

                entity.Property(e => e.Code).HasMaxLength(20);

                entity.Property(e => e.CreateBy).HasMaxLength(1);

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasMaxLength(256)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.IdDistrict).HasColumnName("Id_District");

                entity.Property(e => e.IdProvince).HasColumnName("Id_Province");

                entity.Property(e => e.IdWard).HasColumnName("Id_Ward");

                entity.Property(e => e.NormalizedEmail).HasMaxLength(256);

                entity.Property(e => e.NormalizedUserName).HasMaxLength(256);

                entity.Property(e => e.Role)
                    .HasMaxLength(255)
                    .HasDefaultValueSql("((3))");

                entity.Property(e => e.RoleName).HasMaxLength(255);

                entity.Property(e => e.UserName).HasMaxLength(256);

                entity.HasMany(d => d.Roles)
                    .WithMany(p => p.Users)
                    .UsingEntity<Dictionary<string, object>>(
                        "AspNetUserRole",
                        l => l.HasOne<AspNetRole>().WithMany().HasForeignKey("RoleId"),
                        r => r.HasOne<AspNetUser>().WithMany().HasForeignKey("UserId"),
                        j =>
                        {
                            j.HasKey("UserId", "RoleId");

                            j.ToTable("AspNetUserRoles");

                            j.HasIndex(new[] { "RoleId" }, "IX_AspNetUserRoles_RoleId");
                        });
            });

            modelBuilder.Entity<AspNetUserClaim>(entity =>
            {
                entity.HasIndex(e => e.UserId, "IX_AspNetUserClaims_UserId");

                entity.Property(e => e.ClaimType).IsRequired();

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserClaims)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_AU");
            });

            modelBuilder.Entity<AspNetUserLogin>(entity =>
            {
                entity.HasKey(e => new { e.LoginProvider, e.ProviderKey });

                entity.HasIndex(e => e.UserId, "IX_AspNetUserLogins_UserId");

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserLogins)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserToken>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.LoginProvider, e.Name });

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserTokens)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<Group>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasMaxLength(255)
                    .HasComment("mã\n");

                entity.Property(e => e.CreateBy).HasMaxLength(255);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(256);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasDefaultValueSql("('')");
            });

            modelBuilder.Entity<Search>(entity =>
            {
                entity.ToTable("SEARCH");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("CODE");

                entity.Property(e => e.Createby)
                    .HasMaxLength(100)
                    .HasColumnName("CREATEBY");

                entity.Property(e => e.Createdate)
                    .HasColumnType("date")
                    .HasColumnName("CREATEDATE");

                entity.Property(e => e.Deleted)
                    .HasColumnName("DELETED")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .HasColumnName("DESCRIPTION");

                entity.Property(e => e.Exporttype)
                    .HasMaxLength(100)
                    .HasColumnName("EXPORTTYPE");

                entity.Property(e => e.Keyword)
                    .HasMaxLength(10)
                    .HasColumnName("KEYWORD");

                entity.Property(e => e.Lastchangeby)
                    .HasMaxLength(100)
                    .HasColumnName("LASTCHANGEBY");

                entity.Property(e => e.Lastchangedate)
                    .HasColumnType("date")
                    .HasColumnName("LASTCHANGEDATE");

                entity.Property(e => e.Storename)
                    .HasMaxLength(100)
                    .HasColumnName("STORENAME");

                entity.Property(e => e.Title)
                    .HasMaxLength(500)
                    .HasColumnName("TITLE");

                entity.Property(e => e.Titleen)
                    .HasMaxLength(500)
                    .HasColumnName("TITLEEN");

                entity.Property(e => e.Tltxcd)
                    .HasMaxLength(20)
                    .HasColumnName("TLTXCD");
            });

            modelBuilder.Entity<Searchfld>(entity =>
            {
                entity.ToTable("SEARCHFLD");

                entity.Property(e => e.Align)
                    .HasMaxLength(3)
                    .HasColumnName("ALIGN");

                entity.Property(e => e.Allowsort)
                    .HasMaxLength(3)
                    .HasColumnName("ALLOWSORT")
                    .HasDefaultValueSql("('N')");

                entity.Property(e => e.Control)
                    .HasMaxLength(20)
                    .HasColumnName("CONTROL");

                entity.Property(e => e.Createby)
                    .HasMaxLength(100)
                    .HasColumnName("CREATEBY");

                entity.Property(e => e.Createdate)
                    .HasColumnType("date")
                    .HasColumnName("CREATEDATE");

                entity.Property(e => e.Datatype)
                    .HasMaxLength(10)
                    .HasColumnName("DATATYPE");

                entity.Property(e => e.Defvalue)
                    .HasMaxLength(200)
                    .HasColumnName("DEFVALUE");

                entity.Property(e => e.Display)
                    .HasMaxLength(3)
                    .HasColumnName("DISPLAY")
                    .HasDefaultValueSql("('Y')");

                entity.Property(e => e.Expwidth)
                    .HasColumnName("EXPWIDTH")
                    .HasDefaultValueSql("((80))");

                entity.Property(e => e.Fixed)
                    .HasMaxLength(3)
                    .HasColumnName("FIXED");

                entity.Property(e => e.Fldcode)
                    .HasMaxLength(100)
                    .HasColumnName("FLDCODE");

                entity.Property(e => e.Fldname)
                    .HasMaxLength(200)
                    .HasColumnName("FLDNAME");

                entity.Property(e => e.Fldnameen)
                    .HasMaxLength(200)
                    .HasColumnName("FLDNAMEEN");

                entity.Property(e => e.Isrecordkey)
                    .HasMaxLength(3)
                    .HasColumnName("ISRECORDKEY")
                    .HasDefaultValueSql("('N')");

                entity.Property(e => e.Lastchangeby)
                    .HasMaxLength(100)
                    .HasColumnName("LASTCHANGEBY");

                entity.Property(e => e.Lastchangedate)
                    .HasColumnType("date")
                    .HasColumnName("LASTCHANGEDATE");

                entity.Property(e => e.Operator)
                    .HasMaxLength(25)
                    .HasColumnName("OPERATOR");

                entity.Property(e => e.Position)
                    .HasColumnName("POSITION")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Searchcode)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("SEARCHCODE");

                entity.Property(e => e.Source)
                    .HasMaxLength(100)
                    .HasColumnName("SOURCE");

                entity.Property(e => e.Sqlcmd)
                    .HasMaxLength(4000)
                    .HasColumnName("SQLCMD");

                entity.Property(e => e.Srch)
                    .HasMaxLength(1)
                    .HasColumnName("SRCH")
                    .HasDefaultValueSql("('N')");

                entity.Property(e => e.Width)
                    .HasColumnName("WIDTH")
                    .HasDefaultValueSql("((80))");
            });

            modelBuilder.Entity<UserGroup>(entity =>
            {
                entity.HasKey(e => new { e.IdUser, e.IdGroup })
                    .HasName("PK__UserGrou__7ADBF00D57AD750C");

                entity.Property(e => e.IdUser).HasColumnName("Id_User");

                entity.Property(e => e.IdGroup)
                    .HasMaxLength(255)
                    .HasColumnName("Id_Group");

                entity.Property(e => e.LastUpdateBy).HasMaxLength(255);

                entity.Property(e => e.LastUpdateDate).HasColumnType("datetime");

                entity.HasOne(d => d.IdGroupNavigation)
                    .WithMany(p => p.UserGroups)
                    .HasForeignKey(d => d.IdGroup)
                    .HasConstraintName("FK_GROUP_ID");

                entity.HasOne(d => d.IdUserNavigation)
                    .WithMany(p => p.UserGroups)
                    .HasForeignKey(d => d.IdUser)
                    .HasConstraintName("FK_USER_ID");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
